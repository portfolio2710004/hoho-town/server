# 단계 1: Node.js 이미지를 기반으로
FROM node:15.6.0-alpine AS base

# 앱 디렉토리 생성
WORKDIR /opt

# 앱 의존성 설치
# package.json 과 package-lock.json 복사
COPY package*.json ./
COPY .env.production ./
COPY ./dist ./dist

RUN npm install

# 앱 실행을 위한 포트 노출
EXPOSE 80

# 앱 실행
CMD ["npm", "run", "start:prod"]
