import { Column, CreateDateColumn, Entity, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

import { SpriteSheet } from 'src/interface/phaser/sprite.type';

import { SnsEntity } from './sns.entity';
import { WhitelistEntity } from './whitelist.entity';

@Entity({ name: 'users '})
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ comment: '닉네임', length: '20' })
    nickname: string;

    @Column({ comment: '프로필 이미지 URL', length: '100' })
    thumbnail: string;

    @Column({ comment: '성별' })
    gender: string;

    @Column({ comment: '생년월일' })
    birthday: string;

    @Column({ comment: '아바타 타입' })
    spriteSheet: SpriteSheet;

    @CreateDateColumn({ comment: '생성일' })
    createdAt: Date;

    @UpdateDateColumn({ comment: '수정일' })
    updatedAt: Date;

    @OneToOne(() => SnsEntity, e => e.user, { cascade: true })
    sns: SnsEntity;

    @OneToOne(() => WhitelistEntity, e => e.user, { cascade: true })
    whitelist: WhitelistEntity;
}