import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

import { SnsType } from "src/interface/user/sns.type";

import { UserEntity } from "./user.entity";

@Entity({
    name: 'sns'
})
export class SnsEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ comment: 'sns 제공업체 타입' })
    provider: SnsType;

    @Column({ comment: 'sns에 가입된 회원 고유번호' })
    providerId: string;

    @CreateDateColumn({ comment: '생성일' })
    createdAt: Date;

    @UpdateDateColumn({ comment: '수정일' })
    updatedAt: Date;

    @OneToOne(() => UserEntity, { onDelete: 'CASCADE' })
    @JoinColumn()
    user: UserEntity;
}