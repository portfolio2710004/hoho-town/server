import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { UserEntity } from "./user.entity";

@Entity({ 
    name: 'whitelist'
})
export class WhitelistEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ comment: '리프레시토큰', unique: true })
    refreshToken: string;
    
    @CreateDateColumn({ comment: '생성일' })
    createdAt: Date;

    @UpdateDateColumn({ comment: '수정일' })
    updatedAt: Date;

    @OneToOne(() => UserEntity, { onDelete: 'CASCADE' })
    @JoinColumn()
    user: UserEntity;
}