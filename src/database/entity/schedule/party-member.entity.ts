import { CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

import { UserEntity } from "../user/user.entity";

import { ScheduleEntity } from "./schedule.entity";

@Entity({
    name: 'party_members'
})
export class PartyMemberEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @CreateDateColumn({ comment: '생성일' })
    createdAt: Date;

    @UpdateDateColumn({ comment: '수정일' })
    updatedAt: Date;

    @ManyToOne(() => UserEntity, { onDelete: 'CASCADE' })
    user: UserEntity;

    @ManyToOne(() => ScheduleEntity, { onDelete: 'CASCADE' })
    schedule: ScheduleEntity;
}