import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

import { PartyMemberEntity } from "./party-member.entity";

@Entity({
    name: 'schedules'
})
export class ScheduleEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ comment: '시작일' })
    startDate: Date;

    @Column({ comment: '마감일' })
    endDate: Date;

    @Column({ comment: '제목' })
    title: string;

    @Column({ comment: '메모' })
    memo: string;

    @CreateDateColumn({ comment: '생성일' })
    createdAt: Date;

    @UpdateDateColumn({ comment: '수정일' })
    updatedAt: Date;

    @OneToMany(() => PartyMemberEntity, e => e.schedule)
    partyMembers?: PartyMemberEntity[];
}