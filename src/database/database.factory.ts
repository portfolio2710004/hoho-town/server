import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from "@nestjs/typeorm";

import { SnakeNamingStrategy } from "typeorm-naming-strategies";

@Injectable()
export class DatabaseFactory implements TypeOrmOptionsFactory {

    constructor(
        private readonly configService: ConfigService
    ) { }

    createTypeOrmOptions(): TypeOrmModuleOptions {
        return {
            type: 'postgres',
            extra: {
                charset: 'utf8mb4_unicode_ci'
            },
            host: this.configService.get('DB_HOST'),
            port: this.configService.get('DB_PORT'),
            username: this.configService.get('DB_USERNAME'),
            password: this.configService.get('DB_PASSWORD'),
            database: this.configService.get('DB_NAME'),
            logging: this.configService.get('DB_LOGGING'),
            logger: 'advanced-console',
            entities: [
                `${__dirname}/entity/*.entity.{ts,js}`,
                `${__dirname}/entity/**/*.entity.{ts,js}`
            ],
            autoLoadEntities: true,
            synchronize: this.configService.get('DB_SYNC'),
            namingStrategy: new SnakeNamingStrategy()
        }
    }
}