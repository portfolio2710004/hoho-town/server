import { IAction } from "./event.type";

export interface IEventService {
    dispatch<R, T = any>(action: IAction<T>): Promise<R>;
}