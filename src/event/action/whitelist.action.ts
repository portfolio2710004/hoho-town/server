import { UserEntity } from "src/database/entity/user/user.entity";
import { IAction } from "../event.type";

export type CreateWhitelistActionDto = {
    user: UserEntity;
    refreshToken: string;
}

export class VerifyWhitelistAction implements IAction<number> {
    static readonly type = '[Whitelist] 회원번호로 화이트리스트 검증';
    constructor(public readonly payload: number) {}
}

export class CreateWhitelistAction implements IAction<CreateWhitelistActionDto> {
    static readonly type = '[Whitelist] 화이트리스트 생성';
    constructor(public readonly payload: CreateWhitelistActionDto) {}
}

export class RemoveWhitelistAction implements IAction<number> {
    static readonly type = '[Whitelist] 회원번호로 화이트리스트 삭제';
    constructor(public readonly payload: number) {}
}