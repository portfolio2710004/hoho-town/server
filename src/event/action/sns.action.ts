import { SnsType } from "src/interface/user/sns.type";

import { IAction } from "../event.type";

export type SnsAuthenticateActionDto = {
    provider: SnsType;
    token: string;
}

export class SnsAuthenticateAction implements IAction<SnsAuthenticateActionDto> {
    static readonly type: string = '[Sns] 인증토큰으로 소셜 사용자 조회';
    constructor(public readonly payload: SnsAuthenticateActionDto) {}
}