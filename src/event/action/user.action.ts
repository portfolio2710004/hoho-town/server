import { IAction } from "../event.type";

export class GetUserAction implements IAction<number> {
    static readonly type = '[User] 아이디로 사용자 조회';
    constructor(public readonly payload: number) {}
}

export class GetSnsUserAction implements IAction<string> {
    static readonly type = '[User] 프로바이더 아이디로 sns 사용자 조회';
    constructor(public readonly payload: string) {}
}