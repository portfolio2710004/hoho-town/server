import { OnEvent } from "@nestjs/event-emitter";

export function Action(actionClass: { type: string }) {
    return function(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        OnEvent(actionClass.type)(target, propertyKey, descriptor);
    };
}