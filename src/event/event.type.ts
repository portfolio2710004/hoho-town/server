export type IAction<T=any> = {
    payload?: T;
}