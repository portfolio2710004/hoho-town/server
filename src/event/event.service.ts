import { HttpException, Injectable, Logger } from "@nestjs/common";
import { EventEmitter2 } from "@nestjs/event-emitter";

import { IEventService } from "./event.service.interface";
import { IAction } from "./event.type";

@Injectable()
export class EventService implements IEventService {

    constructor(
        private readonly eventEmitter: EventEmitter2
    ) { }

    async dispatch<R, T>(action: IAction<T>): Promise<R> {
        const actionClass = action.constructor as T as { type: string };

        const actionRes = new Promise(async (resolve, reject) => {
            const result = await this.eventEmitter.emitAsync(
                actionClass.type, action.payload,
                (error: HttpException) => {
                    reject(error);
                });
            resolve(result[0] as R);
        });

        try {
            const result = await actionRes;
            return result as R;
        } catch (error) {
            Logger.error('이벤트 에러 발생');
            throw error;
        }
    }
}