import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";

import { AppModule } from "./app/app.module";
import { AuthModule } from "./auth/auth.module";
import { CoreModule } from "./core/core.module";
import { DatabaseModule } from "./database/database.module";
import { EventModule } from "./event/event.module";
import { WebsocketModule } from "./websocket/websocket.module";

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
            envFilePath: `.env.${process.env.NODE_ENV}`,
        }),
        CoreModule,
        AuthModule,
        DatabaseModule,
        AppModule,
        EventModule,
        WebsocketModule
    ]
})
export class MainModule {}