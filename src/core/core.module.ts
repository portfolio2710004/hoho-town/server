import { BadRequestException, Global, Module, ValidationPipe } from "@nestjs/common";
import { APP_FILTER, APP_PIPE } from "@nestjs/core";

import { ValidationError } from "class-validator";

import { CustomExceptionFilter } from "./filter/custom-exception.filter";

@Global()
@Module({
  providers: [
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({
        transform: true,
        whitelist: true,
        exceptionFactory: (errors: ValidationError[]) => {
          if (errors && errors.length > 0) {
            const error = errors[0].constraints;
            const keys = Object.keys(error);
            const type = keys[keys.length - 1];
            const message = error[type];
            return new BadRequestException(message);
          }
        },
      }),
    },
    {
      provide: APP_FILTER,
      useClass: CustomExceptionFilter,
    },
  ]
})
export class CoreModule { }