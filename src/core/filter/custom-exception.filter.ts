import {
    ArgumentsHost,
    Catch,
    ExceptionFilter,
    GatewayTimeoutException,
    HttpException,
    InternalServerErrorException,
    Logger,
} from '@nestjs/common';

@Catch(HttpException)
export class CustomExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const status = exception.getStatus();

        let errorMessage: string;
        // 500 번대 에러만 처리
        if (status >= 500 && status < 600) {
            errorMessage = this.getErrorMessage(exception);
        } else {
            errorMessage = exception.message;
        }

        Logger.error(`[${status}] ${errorMessage}`, exception.stack);

        response
            .status(status)
            .json({
                statusCode: status,
                message: errorMessage,
            });
    }

    private getErrorMessage(exception: HttpException): string {
        // 커스텀 로직으로 에러 메시지 결정
        // 예시: exception 객체에 따라 다른 메시지 반환
        if (exception instanceof InternalServerErrorException) {
            return '서버 내부 오류가 발생했습니다.';
        } else if (exception instanceof GatewayTimeoutException) {
            return '게이트웨이 시간 초과 오류가 발생했습니다.';
        } else {
            return '서버에서 오류가 발생했습니다.';
        }
    }
}
