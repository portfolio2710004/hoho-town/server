import { ITokens } from "./auth.type";

export type IRefreshRequest = {
  refreshToken: string;
}

export type IRefreshResponse = ITokens;