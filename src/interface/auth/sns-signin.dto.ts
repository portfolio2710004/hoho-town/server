import { SnsType } from "../user/sns.type";

import { ITokens } from "./auth.type";

export type ISnsSigninRequest = {
  provider: SnsType;
  token: string;
}

export type ISnsSigninResponse = ITokens;