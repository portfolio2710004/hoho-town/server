export type ResponseErrorType = {
    error: {
        statusCode: number;
        message: string;
    };
    status: number;
}