import { ITokens } from "../auth/auth.type";

export type IGetUserDetailResponse = ITokens;