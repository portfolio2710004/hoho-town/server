import { SpriteSheet } from "../phaser/sprite.type";
import { Gender } from "../user/user.type";

export type IGetMyProfileResponse = {
    id: number;
    nickname: string;
    gender: Gender;
    birthday: string;
    thumbnail: string;
    spriteSheet: SpriteSheet;
    createdAt: Date;
}