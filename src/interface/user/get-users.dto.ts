export type IGetUsersResponse = {
    id: number;
    nickname: string;
    thumbnail: string;
}