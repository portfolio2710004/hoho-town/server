export enum Genders {
    FEMALE='F',
    MALE='M'
}

export type Gender = `${Genders}`;

export function getGenderArray(): string[] {
    return Object.values(Genders);
}