import { ITokens } from "../auth/auth.type";
import { SpriteSheet } from "../phaser/sprite.type";

import { SnsType } from "./sns.type";
import { Gender } from "./user.type";

export type ICreateSnsUserRequest = {
  provider: SnsType;
  token: string;
  nickname: string;
  birthday: string;
  gender: Gender;
  spriteSheet: SpriteSheet;
}

export type ICreateSnsUserResponse = ITokens;