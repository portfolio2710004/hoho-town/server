import { IGetMyProfileResponse } from "./get-my-profile.dto";

export type IUpdateUserThumbnailRequest = {
    thumbnail: string;
}

export type IUpdateUserThumbnailResponse = IGetMyProfileResponse;