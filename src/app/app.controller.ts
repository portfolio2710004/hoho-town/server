import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

import { Public } from 'src/auth/auth.decorator';

@ApiTags('애플리케이션')
@Controller()
export class AppController {

  @Get()
  @Public()
  @ApiOperation({ summary: '서버 상태 채크 👻' })
  healthy(): string {
    return 'server is running 👻';
  }
}
