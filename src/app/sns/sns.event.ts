import { HttpException, Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { SnsAuthenticateAction, SnsAuthenticateActionDto } from 'src/event/action/sns.action';
import { Action } from 'src/event/event.decorator';
import { ISnsUser, SnsType } from 'src/interface/user/sns.type';

import { KakaoAuthStrategy } from './strategy/kakao-auth.strategy';
import { SnsAuthStrategy } from './strategy/sns-auth-strategy.type';

@Injectable()
export class SnsEvent {

  private readonly strategies: Map<SnsType, SnsAuthStrategy> = new Map();

  constructor(
    private readonly configService: ConfigService
  ) {
    this.strategies.set('kakao', new KakaoAuthStrategy());
  }

  @Action(SnsAuthenticateAction)
  async authenticate(dto: SnsAuthenticateActionDto, callBack?: (error: HttpException) => void) {
    let result: ISnsUser;
    try {
      result = await this.strategies.get(dto.provider).authenticate(dto.token);
    } catch (error) {
      return callBack(error);
    }

    if (!result) {
      return callBack(new NotFoundException('소셜 서비스에 가입된 사용자를 찾을 수 없습니다.'));
    }

    return result;
  }
}
