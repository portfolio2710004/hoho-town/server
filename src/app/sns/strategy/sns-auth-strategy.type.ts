import { ISnsUser } from "src/interface/user/sns.type";

export interface SnsAuthStrategy {
    authenticate(token: string): Promise<ISnsUser>;
}