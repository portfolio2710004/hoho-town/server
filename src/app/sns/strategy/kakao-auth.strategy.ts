import { Logger } from "@nestjs/common";

import axios from 'axios';

import { ISnsUser } from "src/interface/user/sns.type";

import { SnsAuthStrategy } from "./sns-auth-strategy.type";

export class KakaoAuthStrategy implements SnsAuthStrategy {

  private readonly KAKAO_AUTH_URI = 'https://kapi.kakao.com/v2/user/me';

  async authenticate(token: string): Promise<ISnsUser> {
    let result: any;

    try {
      const response = await axios.get(this.KAKAO_AUTH_URI, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        }
      });
      result = response.data;
    } catch (error) {
      Logger.error('소셜인증 서비스 요청 에러');
      return undefined;
    }

    return {
      provider: 'kakao',
      providerId: result.id,
      nickname: result?.properties?.nickname ?? '',
      thumbnail: result?.properties?.thumbnail_image ?? ''
    };
  }
}