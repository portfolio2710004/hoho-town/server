import { Module } from "@nestjs/common";

import { SnsEvent } from "./sns.event";

@Module({
  providers: [
    SnsEvent
  ]
})
export class SnsModule {}