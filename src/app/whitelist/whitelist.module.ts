import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { WhitelistEntity } from "src/database/entity/user/whitelist.entity";

import { WhitelistEvent } from "./whitelist.event";
import { WhitelistRepository } from "./whitelist.repository";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            WhitelistEntity
        ])
    ],
    providers: [
        WhitelistEvent,
        { provide: 'WhitelistRepository', useClass: WhitelistRepository }
    ]
})
export class WhitelistModule { }