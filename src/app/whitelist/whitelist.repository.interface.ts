import { UserEntity } from "src/database/entity/user/user.entity";
import { WhitelistEntity } from "src/database/entity/user/whitelist.entity";

export interface IWhitelistRepository {
    findWhitelistByUserId(userId: number): Promise<WhitelistEntity>;
    findWhitelistByToken(refreshToken: string): Promise<WhitelistEntity>;
    createWhitelist(user: UserEntity, refreshToken: string): Promise<WhitelistEntity>;
    updateWhitelist(whitelist: WhitelistEntity, refreshToken: string): Promise<WhitelistEntity>;
    removeWhitelist(whitelist: WhitelistEntity): Promise<WhitelistEntity>;
}