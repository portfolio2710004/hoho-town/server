import { InjectRepository } from "@nestjs/typeorm";

import { Repository } from "typeorm";

import { UserEntity } from "src/database/entity/user/user.entity";
import { WhitelistEntity } from "src/database/entity/user/whitelist.entity";

import { IWhitelistRepository } from "./whitelist.repository.interface";

export class WhitelistRepository implements IWhitelistRepository {

    constructor(
        @InjectRepository(WhitelistEntity)
        private readonly whitelistRepository: Repository<WhitelistEntity>
    ) {}

    findWhitelistByUserId(userId: number): Promise<WhitelistEntity> {
        return this.whitelistRepository.findOne({
            where: {
                user: {
                    id: userId
                }
            }
        });
    }

    findWhitelistByToken(refreshToken: string): Promise<WhitelistEntity> {
        return this.whitelistRepository.findOne({
            where: {
                refreshToken
            }
        });
    }

    createWhitelist(user: UserEntity, refreshToken: string): Promise<WhitelistEntity> {
        const newWhitelist = this.whitelistRepository.create({
            user, 
            refreshToken
        });
        return this.whitelistRepository.save(newWhitelist);
    }

    updateWhitelist(whitelist: WhitelistEntity, refreshToken: string): Promise<WhitelistEntity> {
        const updateWhitelist = this.whitelistRepository.create({
            ...whitelist,
            refreshToken
        });
        return this.whitelistRepository.save(updateWhitelist);
    }

    removeWhitelist(whitelist: WhitelistEntity): Promise<WhitelistEntity> {
        return this.whitelistRepository.remove(whitelist);
    }
}