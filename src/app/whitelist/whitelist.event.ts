import { HttpException, Inject, Injectable, UnauthorizedException } from "@nestjs/common";

import { CreateWhitelistAction, CreateWhitelistActionDto, VerifyWhitelistAction, RemoveWhitelistAction } from "src/event/action/whitelist.action";
import { Action } from "src/event/event.decorator";

import { IWhitelistRepository } from "./whitelist.repository.interface";

@Injectable()
export class WhitelistEvent {

    constructor(
        @Inject('WhitelistRepository')
        private readonly whitelistRepository: IWhitelistRepository
    ) { }

    @Action(VerifyWhitelistAction)
    async verifyWhitelistByUserId(userId: number, callBack: (error: HttpException) => void) {
        const hasWhitelist = await this.whitelistRepository.findWhitelistByUserId(userId);
        if (!hasWhitelist) {
            return callBack(new UnauthorizedException('사용자 토큰을 찾을 수 없습니다.'));
        }
    }

    @Action(CreateWhitelistAction)
    async create({ user, refreshToken }: CreateWhitelistActionDto) {
        const hasWhitelist = await this.whitelistRepository.findWhitelistByUserId(user.id);
        if (hasWhitelist) {
            return this.whitelistRepository.updateWhitelist(hasWhitelist, refreshToken);
        }
        return this.whitelistRepository.createWhitelist(user, refreshToken);
    }

    @Action(RemoveWhitelistAction)
    async removeByUserId(userId: number) {
        const result = await this.whitelistRepository.findWhitelistByUserId(userId);
        if (!result) {
            return null;
        }
        return this.whitelistRepository.removeWhitelist(result);
    }
}