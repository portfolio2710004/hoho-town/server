import { Inject, Injectable } from "@nestjs/common";

import { IAuthService } from "src/auth/auth.service.interface";
import { UserEntity } from "src/database/entity/user/user.entity";
import { WhitelistEntity } from "src/database/entity/user/whitelist.entity";
import { SnsAuthenticateAction } from "src/event/action/sns.action";
import { GetSnsUserAction } from "src/event/action/user.action";
import { RemoveWhitelistAction, VerifyWhitelistAction } from "src/event/action/whitelist.action";
import { IEventService } from "src/event/event.service.interface";
import { ISnsUser } from "src/interface/user/sns.type";

import { SnsSigninRequest } from "./dto/sns-signin.dto";

@Injectable()
export class OAuthService {
    
    constructor(
        @Inject('EventService')
        private readonly eventService: IEventService,
        @Inject('AuthService')
        private readonly authService: IAuthService
    ) { }

    async snsSignin(request: SnsSigninRequest) {
        const snsUser = await this.eventService.dispatch<ISnsUser>(new SnsAuthenticateAction(request));
        const user = await this.eventService.dispatch<UserEntity>(new GetSnsUserAction(snsUser.providerId));
        return this.authService.sign(user);
    }

    signout(user: UserEntity) {
        return this.eventService.dispatch<WhitelistEntity>(new RemoveWhitelistAction(user.id))
    }

    async refresh(user: UserEntity) {
        await this.eventService.dispatch(new VerifyWhitelistAction(user.id));
        return this.authService.sign(user);
    }
}