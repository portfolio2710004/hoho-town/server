import { UseGuards, applyDecorators } from "@nestjs/common";
import { ApiBearerAuth, ApiBody, ApiOkResponse, ApiOperation } from "@nestjs/swagger";

import { Public } from "src/auth/auth.decorator";
import { RefreshGuard } from "src/auth/guard/refresh.guard";


export function ApiSnsSignin() {
    return applyDecorators(
        Public(),
        ApiOperation({ summary: 'sns 로그인' }),
        ApiBody({
            schema: {
                properties: {
                    provider: { type: 'string', default: 'kakao' },
                    token: { type: 'string', default: 'provider token string' },
                }
            }
        }),
        ApiOkResponse({
            schema: {
                properties: {
                    accessToken: { type: 'string', default: 'token string' },
                    refreshToken: { type: 'string', default: 'token string' },
                }
            }
        })
    )
}

export function ApiSignout() {
    return applyDecorators(
        ApiBearerAuth(),
        ApiOperation({ summary: '로그아웃' })
    );
}

export function ApiRefresh() {
    return applyDecorators(
        Public(),
        UseGuards(RefreshGuard),
        ApiBearerAuth(),
        ApiOperation({ summary: '토큰 재발급' }),
        ApiOkResponse({
            schema: {
                properties: {
                    accessToken: { type: 'string', default: 'token string' },
                    refreshToken: { type: 'string', default: 'token string' },
                }
            }
        })
    )
}