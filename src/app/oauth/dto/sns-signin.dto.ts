import { Exclude, Expose } from "class-transformer";
import { IsIn, IsNotEmpty } from "class-validator";
import { ISnsSigninRequest, ISnsSigninResponse } from "src/interface/auth/sns-signin.dto";

import { SnsType } from "src/interface/user/sns.type";


export class SnsSigninRequest implements ISnsSigninRequest {
    @IsIn(['kakao','naver','google','apple','facebook'], { message: '프로바이더 타입을 입력해 주세요.'})
    provider: SnsType;

    @IsNotEmpty({ message: '토큰을 입력해 주세요.' })
    token: string;
}

@Exclude()
export class SnsSigninResponse implements ISnsSigninResponse {
    @Expose()
    accessToken: string;
    @Expose()
    refreshToken: string;
}