import { Exclude, Expose } from "class-transformer";

import { IRefreshResponse } from "src/interface/auth/refresh.dto";

@Exclude()
export class RefreshResponse implements IRefreshResponse {
    @Expose()
    accessToken: string;
    @Expose()
    refreshToken: string;
}