import { Body, Controller, Get, HttpStatus, Post, Res } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";

import { plainToInstance } from "class-transformer";

import { Response } from "express";

import { User } from "src/auth/auth.decorator";
import { UserEntity } from "src/database/entity/user/user.entity";

import { RefreshResponse } from "./dto/refresh.dto";
import { SnsSigninRequest, SnsSigninResponse } from "./dto/sns-signin.dto";
import { ApiRefresh, ApiSignout, ApiSnsSignin } from "./oauth.docs";
import { OAuthService } from "./oauth.service";

@ApiTags('인증')
@Controller({ 
    version: '1'
})
export class OAuthController {

    constructor(
        private readonly oauthService: OAuthService
    ) { }

    @Post('sns')
    @ApiSnsSignin()
    async snsSignin(@Body() request: SnsSigninRequest) {
        const result = await this.oauthService.snsSignin(request);
        return plainToInstance(SnsSigninResponse, result);
    }

    @Get('signout')
    @ApiSignout()
    async signout(@User() user: UserEntity, @Res() response: Response) {
        await this.oauthService.signout(user);
        return response.status(HttpStatus.NO_CONTENT).send();
    }

    @Post('refresh')
    @ApiRefresh()
    async refresh(@User() user: UserEntity) {
        const result = await this.oauthService.refresh(user);
        return plainToInstance(RefreshResponse, result);
    }
}