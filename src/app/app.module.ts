import { Module } from '@nestjs/common';
import { RouterModule, Routes } from '@nestjs/core';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OAuthModule } from './oauth/oauth.module';
import { SnsModule } from './sns/sns.module';
import { UserModule } from './user/user.module';

const routes: Routes = [
  {
    path: 'oauth',
    module: OAuthModule
  },
  {
    path: 'users',
    module: UserModule
  }
];

@Module({
  imports: [
    RouterModule.register(routes),
    OAuthModule,
    UserModule,
    SnsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
