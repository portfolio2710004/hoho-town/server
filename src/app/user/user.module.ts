import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { UserEntity } from "src/database/entity/user/user.entity";

import { UserController } from "./user.controller";
import { UserEvent } from "./user.event";
import { UserRepository } from "./user.repository";
import { UserService } from "./user.service";

@Module({ 
    imports: [
        TypeOrmModule.forFeature([
            UserEntity
        ])
    ],
    controllers: [
        UserController
    ],
    providers: [
        UserService,
        UserEvent,
        { provide: 'UserRepository', useClass: UserRepository }
    ]
})
export class UserModule {}