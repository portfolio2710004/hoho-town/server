import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Put, Res } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";

import { Response } from "express";

import { plainToInstance } from "class-transformer";

import { Public, User } from "src/auth/auth.decorator";
import { UserEntity } from "src/database/entity/user/user.entity";

import { CreateSnsUserRequest, CreateSnsUserResponse } from "./dto/create-sns-user.dto";
import { GetMyProfileResponse } from "./dto/get-my-profile.dto";
import { GetUserDetailResponse } from "./dto/get-user-detail.dto";
import { GetUsersResponse } from "./dto/get-users.dto";
import { UpdateUserThumbnailRequest, UpdateUserThumbnailResponse } from "./dto/update-user-thumbnail.dto";
import { UpdateUserRequest, UpdateUserResponse } from "./dto/update-user.dto";
import { ApiCreateSnsUser, ApiGetMyProfile, ApiGetUserDetail, ApiGetUsers, ApiUpdateMyProfile, ApiUpdateMyProfileThumbnail, ApiWithdrawal } from "./user.docs";
import { UserService } from "./user.service";

@ApiTags('회원')
@Controller({
    version: '1'
})
export class UserController {

    constructor(
        private readonly userService: UserService
    ) {}

    @Get('profile')
    @ApiGetMyProfile()
    getMyProfile(@User() user: UserEntity) {
        return plainToInstance(GetMyProfileResponse, user);
    }

    @Get()
    @ApiGetUsers()
    getUsers() {
        const results = this.userService.getUsers();
        return plainToInstance(GetUsersResponse, results);
    }

    @Get(':id')
    @ApiGetUserDetail()
    async getUser(@Param('id') id: number) {
        const result = await this.userService.getUserDetail(id);
        return plainToInstance(GetUserDetailResponse, result);
    }

    @Post()
    @ApiCreateSnsUser()
    async createSnsUser(@Body() request: CreateSnsUserRequest) {
        const result = await this.userService.createSnsUser(request);
        return plainToInstance(CreateSnsUserResponse, result);
    }

    // @Post('test')
    // @Public()
    // async createTestUser(@Body() request: CreateSnsUserRequest) {
    //     const result = await this.userService.createTestUser(request);
    //     return plainToInstance(CreateSnsUserResponse, result);
    // }

    @Put('profile')
    @ApiUpdateMyProfile()
    async updateMyProfile(@User() user: UserEntity, @Body() request: UpdateUserRequest) {
        const result = await this.userService.updateMyProfile(user, request);
        return plainToInstance(UpdateUserResponse, result);
    }

    @Patch('profile/thumbnail')
    @ApiUpdateMyProfileThumbnail()
    async updateMyProfileThumbnail(@User() user: UserEntity, @Body() { thumbnail }: UpdateUserThumbnailRequest) {
        const result = await this.userService.updateMyProfileThumbnail(user, thumbnail);
        return plainToInstance(UpdateUserThumbnailResponse, result);
    }

    @Delete('profile')
    @ApiWithdrawal()
    async withdrawal(@User() user: UserEntity, @Res() response: Response) {
        await this.userService.withdrawal(user);
        return response.status(HttpStatus.NO_CONTENT).send();
    }
}