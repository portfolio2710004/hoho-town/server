import { InjectRepository } from "@nestjs/typeorm";

import { Repository } from "typeorm";

import { UserEntity } from "src/database/entity/user/user.entity";

import { CreateSnsUserDto } from "./dto/create-sns-user.dto";
import { UpdateUserRequest } from "./dto/update-user.dto";
import { IUserRepository } from "./user.repository.interface";

export class UserRepository implements IUserRepository {

    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>
    ) {}

    findUsers() {
        return this.userRepository.find({
            order: { 
                createdAt: 'DESC'
            }
        });
    }

    findUserById(id: number): Promise<UserEntity> {
        return this.userRepository.findOne({
            where: {
                id
            }
        });
    }

    findUserByProviderId(providerId: string): Promise<UserEntity> {
        return this.userRepository.findOne({
            where: {
                sns: {
                    providerId
                }
            }
        });
    }

    createSnsUser(dto: CreateSnsUserDto): Promise<UserEntity> {
        const newUser =  this.userRepository.create({
            nickname: dto.nickname,
            birthday: dto.birthday,
            thumbnail: dto.thumbnail,
            gender: dto.gender,
            spriteSheet: dto.spriteSheet,
            sns: {
                provider: dto.provider,
                providerId: dto.providerId
            }
        });
        return this.userRepository.save(newUser);
    }

    updateUser(user: UserEntity, request: UpdateUserRequest): Promise<UserEntity> {
        const updateUser = this.userRepository.create({
            ...user,
            ...request
        });
        return this.userRepository.save(updateUser);
    }

    updateUserThumbnail(user: UserEntity, thumbnail: string): Promise<UserEntity> {
        const updateUser = this.userRepository.create({
            ...user,
            thumbnail
        });
        return this.userRepository.save(updateUser);
    }

    removeUser(user: UserEntity): Promise<UserEntity> {
        return this.userRepository.remove(user);
    }
}