import { Exclude, Expose } from "class-transformer";

import { IGetUsersResponse } from "src/interface/user/get-users.dto";

@Exclude()
export class GetUsersResponse implements IGetUsersResponse{
    @Expose()
    id: number;

    @Expose()
    nickname: string;

    @Expose()
    thumbnail: string;
}