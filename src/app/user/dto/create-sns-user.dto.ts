import { Exclude, Expose } from "class-transformer";
import { IsIn, IsNotEmpty } from "class-validator";

import { SpriteSheet, getSpriteSheetArray } from "src/interface/phaser/sprite.type";
import { ICreateSnsUserRequest, ICreateSnsUserResponse } from "src/interface/user/create-sns-user.dto";
import { SnsType, getSnsArray } from "src/interface/user/sns.type";
import { Gender, getGenderArray } from "src/interface/user/user.type";

export class CreateSnsUserRequest implements ICreateSnsUserRequest {
    
    @IsIn(getSnsArray(), { message: '프로바이더 타입을 입력해 주세요.'})
    provider: SnsType;

    @IsNotEmpty({ message: '토큰을 입력해 주세요.' })
    token: string;

    @IsNotEmpty({ message: '닉네임을 입력해 주세요.'})
    nickname: string;

    @IsNotEmpty({ message: '생년월일을 입력해 주세요.'})
    birthday: string;

    @IsIn(getGenderArray(), { message: '성별 타입을 입력해 주세요.'})
    gender: Gender;

    @IsIn(getSpriteSheetArray(), { message: '성별 타입을 입력해 주세요.'})
    spriteSheet: SpriteSheet;
}

export class CreateSnsUserDto {
    providerId: string;
    provider: SnsType;
    nickname: string;
    birthday: string;
    gender: Gender;
    thumbnail: string;
    spriteSheet: SpriteSheet;
}

@Exclude()
export class CreateSnsUserResponse implements ICreateSnsUserResponse {
    @Expose()
    accessToken: string;

    @Expose()
    refreshToken: string;
}