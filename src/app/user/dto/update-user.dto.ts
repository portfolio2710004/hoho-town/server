import { IsIn, IsNotEmpty } from "class-validator";

import { Exclude } from "class-transformer";
import { Gender, getGenderArray } from "src/interface/user/user.type";
import { GetMyProfileResponse } from "./get-my-profile.dto";

export class UpdateUserRequest {
    @IsNotEmpty({ message: '닉네임을 입력해 주세요.' })
    nickname: string;

    @IsNotEmpty({ message: '생년월일을 입력해 주세요.' })
    birthday: string;

    @IsIn(getGenderArray(), { message: '성별을 입력해 주세요.' })
    gender: Gender;
}

@Exclude()
export class UpdateUserResponse extends GetMyProfileResponse { }