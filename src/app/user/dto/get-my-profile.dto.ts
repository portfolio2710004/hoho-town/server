import { Exclude, Expose } from "class-transformer";
import { SpriteSheet } from "src/interface/phaser/sprite.type";
import { IGetMyProfileResponse } from "src/interface/user/get-my-profile.dto";

import { Gender } from "src/interface/user/user.type";

@Exclude()
export class GetMyProfileResponse implements IGetMyProfileResponse {
    @Expose()
    id: number;
    
    @Expose()
    nickname: string;
    
    @Expose()
    gender: Gender;
    
    @Expose()
    birthday: string;
    
    @Expose()
    spriteSheet: SpriteSheet;

    @Expose()
    thumbnail: string;

    @Expose()
    createdAt: Date;
}