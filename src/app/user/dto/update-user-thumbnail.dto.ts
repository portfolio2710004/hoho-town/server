import { Exclude } from "class-transformer";

import { GetMyProfileResponse } from "./get-my-profile.dto";

export class UpdateUserThumbnailRequest {
    thumbnail: string;
}

@Exclude()
export class UpdateUserThumbnailResponse extends GetMyProfileResponse { }