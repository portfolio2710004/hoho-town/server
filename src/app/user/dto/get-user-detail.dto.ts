import { Exclude } from "class-transformer";

import { GetMyProfileResponse } from "./get-my-profile.dto";

@Exclude()
export class GetUserDetailResponse extends GetMyProfileResponse { }