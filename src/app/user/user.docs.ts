import { applyDecorators } from "@nestjs/common";
import { ApiOperation } from "@nestjs/swagger";
import { Public } from "src/auth/auth.decorator";

export function ApiGetMyProfile() {
    return applyDecorators(
        ApiOperation({ summary: '인증된 본인 프로필 조회' })
    );
}

export function ApiGetUsers() {
    return applyDecorators(
        ApiOperation({ summary: '호호타운 사용자 목록 조회' })
    );
}

export function ApiGetUserDetail() {
    return applyDecorators(
        ApiOperation({ summary: '다른 사용자 상세 조회' })
    );
}

export function ApiCreateSnsUser() {
    return applyDecorators(
        Public(),
        ApiOperation({ summary: '소셜 사용자 생성' })
    )
}

export function ApiUpdateMyProfile() {
    return applyDecorators(
        ApiOperation({ summary: '내 프로필 정보 수정' })
    );
}

export function ApiUpdateMyProfileThumbnail() {
    return applyDecorators(
        ApiOperation({ summary: '내 프로필 이미지 수정' })
    );
}

export function ApiWithdrawal() {
    return applyDecorators(
        ApiOperation({ summary: '회원탈퇴' })
    );
}