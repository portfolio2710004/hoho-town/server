import { UserEntity } from "src/database/entity/user/user.entity";

import { CreateSnsUserDto } from "./dto/create-sns-user.dto";
import { UpdateUserRequest } from "./dto/update-user.dto";

export interface IUserRepository {
    findUsers(): Promise<UserEntity[]>;
    findUserById(id: number): Promise<UserEntity>;
    findUserByProviderId(providerId: string): Promise<UserEntity>;

    createSnsUser(dto: CreateSnsUserDto): Promise<UserEntity>;

    updateUser(user: UserEntity, request: UpdateUserRequest): Promise<UserEntity>;
    updateUserThumbnail(user: UserEntity, thumbnail: string): Promise<UserEntity>;

    removeUser(user: UserEntity): Promise<UserEntity>;
}