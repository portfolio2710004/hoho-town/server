import { HttpException, Inject, Injectable, NotFoundException } from "@nestjs/common";

import { GetSnsUserAction, GetUserAction } from "src/event/action/user.action";
import { Action } from "src/event/event.decorator";

import { IUserRepository } from "./user.repository.interface";

@Injectable()
export class UserEvent {

    constructor(
        @Inject('UserRepository')
        private readonly userRepository: IUserRepository
    ) {}

    @Action(GetUserAction)
    async getUserById(id: number, callBack: (error: HttpException) => void) {
        const hasUser = await this.userRepository.findUserById(id);
        if (!hasUser) {
            return callBack(new NotFoundException('사용자를 찾을 수 없습니다.'));
        }
        return hasUser;
    }

    @Action(GetSnsUserAction)
    async getUserByProviderId(providerId: string, callBack: (error: HttpException) => void) {
        const hasUser = await this.userRepository.findUserByProviderId(providerId);
        if (!hasUser) {
            return callBack(new NotFoundException('사용자를 찾을 수 없습니다.'));
        }
        return hasUser;
    }
}