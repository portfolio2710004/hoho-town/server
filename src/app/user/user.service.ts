import { ConflictException, Inject, Injectable, NotFoundException } from "@nestjs/common";

import { v4 as uuidv4 } from 'uuid';

import { IAuthService } from "src/auth/auth.service.interface";
import { UserEntity } from "src/database/entity/user/user.entity";
import { SnsAuthenticateAction, SnsAuthenticateActionDto } from "src/event/action/sns.action";
import { IEventService } from "src/event/event.service.interface";
import { ISnsUser } from "src/interface/user/sns.type";

import { CreateSnsUserDto, CreateSnsUserRequest } from "./dto/create-sns-user.dto";
import { UpdateUserRequest } from "./dto/update-user.dto";
import { IUserRepository } from "./user.repository.interface";

@Injectable()
export class UserService {

    constructor(
        @Inject('EventService')
        private readonly eventService: IEventService,
        @Inject('AuthService')
        private readonly authService: IAuthService,
        @Inject('UserRepository')
        private readonly userRepository: IUserRepository,
    ) {}

    getUsers() {
        return this.userRepository.findUsers();
    }

    async getUserDetail(id: number) {
        const hasUser = await this.userRepository.findUserById(id);
        if (!hasUser) {
            throw new NotFoundException('사용자를 찾을 수 없습니다.');
        }
        return hasUser;
    }

    async createSnsUser(request: CreateSnsUserRequest) {
        const dto: SnsAuthenticateActionDto = {
            provider: request.provider,
            token: request.token
        };
        const snsUser = await this.eventService.dispatch<ISnsUser>(new SnsAuthenticateAction(dto));
        const alreadyUser = await this.userRepository.findUserByProviderId(snsUser.providerId);
        if (alreadyUser) {
            throw new ConflictException('서비스에 가입된 소셜 사용자 입니다.');
        }

        const newSnsUser: CreateSnsUserDto = {
            providerId: snsUser.providerId,
            provider: snsUser.provider,
            nickname: request.nickname,
            gender: request.gender,
            birthday: request.birthday,
            spriteSheet: request.spriteSheet,
            thumbnail: snsUser.thumbnail
        };
        const createdUser = await this.userRepository.createSnsUser(newSnsUser);
        return await this.authService.sign(createdUser);
    }

    async createTestUser(request: CreateSnsUserRequest) {
        const randomId = uuidv4();
        const alreadyUser = await this.userRepository.findUserByProviderId(randomId);
        if (alreadyUser) {
            throw new ConflictException('서비스에 가입된 소셜 사용자 입니다.');
        }

        const newSnsUser: CreateSnsUserDto = {
            providerId: randomId,
            provider: 'kakao',
            nickname: request.nickname,
            gender: request.gender,
            birthday: request.birthday,
            spriteSheet: request.spriteSheet,
            thumbnail: ''
        };
        const createdUser = await this.userRepository.createSnsUser(newSnsUser);
        return await this.authService.sign(createdUser);
    }

    updateMyProfile(user: UserEntity, request: UpdateUserRequest) {
        return this.userRepository.updateUser(user, request);
    }

    updateMyProfileThumbnail(user: UserEntity, thumbnail: string) {
        return this.userRepository.updateUserThumbnail(user, thumbnail);
    }

    withdrawal(user: UserEntity) {
        return this.userRepository.removeUser(user);
    }
}