import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';

import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { MainModule } from './main.module';

async function bootstrap() {
  const app = await NestFactory.create(MainModule);

  app.enableCors({
    origin: true,
    methods: '*'
  });

  app.setGlobalPrefix('api', {
    exclude: ['/','/socket.io/']
  });
  app.enableVersioning();

  const swagger = new DocumentBuilder()
    .setTitle('호호타운 🏬')
    .setDescription('호호타운 API 서버')
    .setVersion('0.0.1')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, swagger);
  SwaggerModule.setup('document', app, document);

  const config = app.get(ConfigService);
  const port = config.get('APP_PORT');
  await app.listen(port, () => Logger.log(`application is running on ${port} 🚀`, 'main.ts'));
}
bootstrap();
