import { ExecutionContext, SetMetadata, createParamDecorator } from "@nestjs/common";

export const Public = () => SetMetadata('public', true);

export const User = createParamDecorator<string>((key: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;

    return key ? user && user[key] : user;
});