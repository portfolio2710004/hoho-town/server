import { ExecutionContext, Injectable } from '@nestjs/common';

import { ITokenType } from 'src/interface/auth/auth.type';

import { BaseAuthGuard } from './auth-base.guard';

@Injectable()
export class AuthGuard extends BaseAuthGuard {

  tokenType: ITokenType = 'ACCESS';

  async canActivate(context: ExecutionContext): Promise<boolean> {
    if (this.isPublic(context)) {
      return true;
    }
    return await this.isAuthorization(context);
  }

  private isPublic(context: ExecutionContext) {
    return this.reflector.getAllAndOverride<boolean>('public', [
      context.getHandler(),
      context.getClass(),
    ]);
  }
}
