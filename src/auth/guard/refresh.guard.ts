import { ExecutionContext, Injectable } from '@nestjs/common';

import { ITokenType } from 'src/interface/auth/auth.type';

import { BaseAuthGuard } from './auth-base.guard';

@Injectable()
export class RefreshGuard extends BaseAuthGuard {

  tokenType: ITokenType = 'REFRESH';

  canActivate(context: ExecutionContext): Promise<boolean> {
    return this.isAuthorization(context);
  }
}
