import { CanActivate, ExecutionContext, Inject, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

import { UserEntity } from 'src/database/entity/user/user.entity';
import { GetUserAction } from 'src/event/action/user.action';
import { IEventService } from 'src/event/event.service.interface';
import { ITokenPayload, ITokenType } from 'src/interface/auth/auth.type';

import { IAuthService } from '../auth.service.interface';

export abstract class BaseAuthGuard implements CanActivate {

    abstract tokenType: ITokenType;

    constructor(
        protected readonly reflector: Reflector,
        @Inject('AuthService') private readonly authService: IAuthService,
        @Inject('EventService') private readonly eventService: IEventService
    ) { }

    abstract canActivate(context: ExecutionContext): Promise<boolean>;

    protected async isAuthorization(context: ExecutionContext) {
        const request = context.switchToHttp().getRequest();

        const { authorization } = request.headers;

        if (!authorization) {
            throw new UnauthorizedException('액세스 토큰이 필요한 작업 입니다.');
        }

        const isBearer = authorization.startsWith('Bearer');
        if (!isBearer) {
            throw new UnauthorizedException('Bearer 토큰이 아닙니다.');
        }

        const token = authorization.split(' ').pop();
        if (!token) {
            throw new UnauthorizedException('액세스 토큰을 입력해 주세요.');
        }

        const payload: ITokenPayload = await this.authService.verify(token, this.tokenType);
        request.user = await this.eventService.dispatch<UserEntity>(new GetUserAction(payload.aud));

        return true;
    }
}
