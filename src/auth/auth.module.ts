import { Global, Module } from "@nestjs/common";
import { APP_GUARD } from "@nestjs/core";
import { JwtService } from "@nestjs/jwt";

import { AuthService } from "./auth.service";
import { AuthGuard } from "./guard/auth.guard";
import { RefreshGuard } from "./guard/refresh.guard";

@Global()
@Module({
    providers: [
        JwtService,
        RefreshGuard,
        { provide: 'AuthService', useClass: AuthService },
        { provide: APP_GUARD, useClass: AuthGuard },
    ],
    exports: [
        'AuthService',
        RefreshGuard,
    ]
})
export class AuthModule { }