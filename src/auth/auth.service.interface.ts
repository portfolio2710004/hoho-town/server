import { UserEntity } from "src/database/entity/user/user.entity";
import { ITokenPayload, ITokenType, ITokens } from "src/interface/auth/auth.type";

export interface IAuthService {
    sign(user: UserEntity): Promise<ITokens>;
    verify(token: string, type?: ITokenType): Promise<ITokenPayload>;
}