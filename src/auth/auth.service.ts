import { Inject, Injectable, InternalServerErrorException, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";

import { UserEntity } from "src/database/entity/user/user.entity";
import { WhitelistEntity } from "src/database/entity/user/whitelist.entity";
import { CreateWhitelistAction, CreateWhitelistActionDto } from "src/event/action/whitelist.action";
import { IEventService } from "src/event/event.service.interface";
import { ITokenType, TokenTypes } from "src/interface/auth/auth.type";

import { IAuthService } from "./auth.service.interface";

@Injectable()
export class AuthService implements IAuthService {

    constructor(
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService,
        @Inject('EventService')
        private readonly eventService: IEventService
    ) { }

    async sign(user: UserEntity) {
        const accessToken = this.createTokenByType(user.id, TokenTypes.ACCESS);
        const refreshToken = this.createTokenByType(user.id, TokenTypes.REFRESH);

        const dto: CreateWhitelistActionDto = { user, refreshToken };
        await this.eventService.dispatch<WhitelistEntity>(new CreateWhitelistAction(dto));

        return {
            accessToken,
            refreshToken
        };
    }

    async verify(token: string, type: ITokenType = TokenTypes.ACCESS) {
        try {
            const result = await this.jwtService.verifyAsync(token, {
                secret: this.configService.get(`${type}_TOKEN_SECRET`)
            })
            return result;
        } catch {
            throw new UnauthorizedException('사용할 수 없는 토큰입니다.');
        }
    }

    private createTokenByType(aud: number, type: ITokenType) {
        try {
            const result = this.jwtService.sign({ aud, type }, {
                secret: this.configService.get(`${type}_TOKEN_SECRET`),
                expiresIn: this.configService.get(`${type}_TOKEN_EXPIRES_IN`)
            })
            return result;
        } catch {
            throw new InternalServerErrorException('JWT 토큰 생성에 문제가 발생했습니다.');
        }
    }
}