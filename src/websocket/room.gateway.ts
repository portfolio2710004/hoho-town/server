import { OnGatewayDisconnect, SubscribeMessage, WebSocketGateway } from '@nestjs/websockets';

import { Socket } from 'socket.io';

import { WebSocketEvents } from 'src/interface/phaser/event.type';
import { Player } from 'src/interface/phaser/player.type';

export type PlayerWithSocket = Player & {
  socket: Socket;
}

@WebSocketGateway({
  cors: {
    origin: true
  }
})
export class RoomGateway implements OnGatewayDisconnect {
  private players: Map<string, Player> = new Map();

  handleDisconnect(closedSocket: Socket) {
    // 접속이 끊어진 클라이언트를 제거
    const player = this.players.get(closedSocket.id);
    this.players.delete(closedSocket.id);

    // 다른 클라이언트에게 연결 해제 사실을 알림
    closedSocket.broadcast.to(WebSocketEvents.ROOM).emit(WebSocketEvents.LEAVE_OTHER_PLAYER, player?.id);
  }

  @SubscribeMessage(WebSocketEvents.JOIN)
  handleJoinRoom(newSocket: Socket, newPlayer: Player): void {
    newSocket.join(WebSocketEvents.ROOM);
    this.players.set(newSocket.id, newPlayer);

    const otherPlayers: Player[] = [];
    for (let [socketId, player] of this.players.entries()) {
      if (player.id === newPlayer.id) continue;
      otherPlayers.push(player);
    }

    newSocket.emit(WebSocketEvents.JOINED, [newPlayer, otherPlayers]);
    newSocket.broadcast.to(WebSocketEvents.ROOM).emit(WebSocketEvents.JOIN_OTHER_PLAYERS, newPlayer);
  }

  @SubscribeMessage(WebSocketEvents.UPDATE_PAYER)
  handleUpdatePlayer(newSocket: Socket, player: Player): void {
    newSocket.join(WebSocketEvents.ROOM);
    this.players.set(newSocket.id, player);

    newSocket.broadcast.to(WebSocketEvents.ROOM).emit(WebSocketEvents.UPDATE_OTHER_PLAYERS, player);
  }
}